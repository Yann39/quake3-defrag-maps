# Quake 3 DeFRaG maps

Some maps for the **Quake 3 arena DeFRag** mod.

![Version](https://img.shields.io/badge/Version-1.0.0-2AAB92.svg)
![Static Badge](https://img.shields.io/badge/Last%20update-23%20June%202004-blue)

---

# Table of Contents

* [About](#about)
* [Maps](#maps)
    * [de4th_run1](#de4th_run1)
    * [de4th_run2](#de4th_run2)
    * [acc_fuzzle](#acc_fuzzle)
* [Usage](#usage)
* [License](#license)

# About

<table>
  <tr>
    <td>
      <img src="doc/logo-quake3arena.png" alt="Quake 3 Arena logo" height="128"/>
    </td>
    <td>
      <img src="doc/logo-defrag.png" alt="DeFRaG logo" height="60"/>
    </td>
    <td>
      <img src="doc/logo-radiant.svg" alt="GtkRadiant logo" height="128"/>
    </td>
  </tr>
</table>

Some **Quake 3 arena** maps I created a long time ago (early 2004) for the **DeFRag** mod.

Build with **GTK Radiant 1.5.0** for **DeFRag 1.9**.

> [!NOTE]
> Of course there are much better tools and resources now to build better maps,
> these are not optimized, but they are the original ones :)

# Maps

## de4th_run1

- Type : run
- Physics : VQ3 (Vanilla Quake 3)
- Weapons : <img alt="Logo plasma" src="doc/logo-defrag-plasma.svg" height="16"/> Plasmagun
- Items : <img alt="Logo battlesuit" src="doc/logo-defrag-battlesuit.svg" height="16"/> Battle suit, <img alt="Logo haste" src="doc/logo-defrag-haste.svg" height="16"/> Haste

![de4th_run1](doc/de4th_run1.jpg)

## de4th_run2

- Type : run
- Physics : VQ3 (Vanilla Quake 3)
- Weapons : <img alt="Logo rocket" src="doc/logo-defrag-rocket.svg" height="16"/> Rocket Launcher
- Items : <img alt="Logo battlesuit" src="doc/logo-defrag-battlesuit.svg" height="16"/> Battle suit, <img alt="Logo haste" src="doc/logo-defrag-haste.svg" height="16"/> Haste

![de4th_run2](doc/de4th_run2.jpg)

## acc_fuzzle

- Type : accuracy
- Physics : VQ3 (Vanilla Quake 3), CPM (Challenge Promode)

- Weapons : <img alt="Logo railgun" src="doc/logo-defrag-railgun.svg" height="16"/> Railgun
- Items : <img alt="Logo haste" src="doc/logo-defrag-haste.svg" height="16"/> Haste

![acc_fuzzle](doc/acc_fuzzle.jpg)

# Usage

Download the maps, then just put the `.pk3` files in your _baseq3/maps_ folder.

You can also find the maps online from **q3df.org** :

- https://en.ws.q3df.org/map/De4th_run1/
- https://en.ws.q3df.org/map/De4th_run2/
- https://en.ws.q3df.org/map/acc_fuzzle/

# License

[General Public License (GPL) v3](https://www.gnu.org/licenses/gpl-3.0.en.html)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not,
see <http://www.gnu.org/licenses/>.